﻿[<RequireQualifiedAccess>]
module Reap.Overview

open Android.App
open Android.Content
open Android.Graphics
open Android.OS
open Android.Runtime
open Android.Views
open Android.Widget

open System
open System.Reactive.Subjects

open FSharp.Control.Reactive

open FSActivity
open Shared
open MVUPattern
open Log

type LoadStatus =
    | NotLoading
    | Loading
    | Loaded
    | LoadingFailed of string

type Model =
    { credentials:Harvest.Credentials
    ; dayEntries:Harvest.DailyEntries[]
    ; loadStatus:LoadStatus
    }

type Msg =
    | ModelReloaded of Model
    | LoadData of int * int
    | DataLoadFailed of string
    | DataLoaded of Harvest.DailyEntries[]

let loadMonthEntries credentials year month =
    let now = DateTime.Now.Date
    let entriesAsync = Seq.initInfinite id
                       |> Seq.map (fun i -> now.AddDays(float -i))
                       |> Seq.takeWhile (fun ts -> ts.Month = now.Month)
                       |> Seq.map (Harvest.dayEntries credentials)
                       |> Async.Parallel
    
    async {
        let! entries = entriesAsync
        return DataLoaded (entries |> Array.sortBy (fun e -> e.for_day))
    }

let update model msg =
    match msg with
    | ModelReloaded m -> (m, NoCmd)
    | LoadData (year, month) ->
        let loadFailed (ex: exn) = DataLoadFailed ex.Message
        ({model with loadStatus=Loading}, Execute (loadFailed, loadMonthEntries model.credentials year month))
    | DataLoaded entries -> ({model with loadStatus=Loaded; dayEntries=entries}, NoCmd)
    | DataLoadFailed msg -> ({model with loadStatus=LoadingFailed msg}, NoCmd)


type private CalendarEntry =
    | Entries of int * Harvest.DailyEntries
    | NoEntry of int * DateTime

type DailyEntriesAdapter(activity: Activity, entriesList: Harvest.DailyEntries[]) =
    inherit BaseAdapter()

    let mutable entries = entriesList

    let daysInRowBefore (day: DayOfWeek) =
        match day with
        | DayOfWeek.Sunday -> 6
        | v -> (int v) - 1

    let indexEntry i =
        let first = Array.get entries 0
        let offset = daysInRowBefore first.for_day.DayOfWeek
        if i < offset || i - offset > (entries.Length-1) then
            NoEntry (i, first.for_day.AddDays(float (-offset + i)))
        else
            Entries (i, Array.get entries (i-offset))

    override this.GetItem (pos) = null
    override this.GetItemId (pos) = 0L

    member this.Entries
        with get () = entries
        and  set (v: Harvest.DailyEntries[]) =
             entries <- v
             this.NotifyDataSetChanged()

    override this.GetView (pos, convertView, parent) =
        let layout = match convertView with
                     | null -> new RelativeLayout(activity)
                     | :? RelativeLayout as l ->
                        l.RemoveAllViews()
                        l
                     | _ -> failwith "recycled view is not RelativeLayout"

        let dateText = new TextView(activity)
        let dateLayoutParams = new RelativeLayout.LayoutParams(
                                   RelativeLayout.LayoutParams.MatchParent,
                                   RelativeLayout.LayoutParams.WrapContent)
        dateLayoutParams.AddRule(LayoutRules.AlignParentTop)
        dateText.LayoutParameters <- dateLayoutParams

        let hoursText = new TextView(activity)
        let hoursLayoutParams = new RelativeLayout.LayoutParams(
                                    RelativeLayout.LayoutParams.MatchParent,
                                    RelativeLayout.LayoutParams.WrapContent)
        hoursLayoutParams.AddRule(LayoutRules.AlignParentBottom)
        hoursLayoutParams.AddRule(LayoutRules.AlignParentRight)
        hoursText.SetBackgroundColor(new Color(0xDDDDDDDD))
        hoursText.LayoutParameters <- hoursLayoutParams

        layout.AddView(dateText)
        layout.AddView(hoursText)

        let entry = indexEntry pos
        match entry with
        | Entries (i, entries) ->
            let total = entries.day_entries |> List.map (fun e -> e.hours) |> List.sum
            let textColor = new Color(0xFFFFFFFF)
            dateText.SetTextColor(textColor)
            dateText.Text <- entries.for_day.Day.ToString()
            hoursText.SetTextColor(textColor)
            hoursText.Text <- sprintf "%.1fh" total
        | NoEntry (i, date) ->
            let textColor = new Color(0xBBBBBBBB)
            dateText.Text <- date.Day.ToString()
            dateText.SetTextColor(textColor)
            hoursText.SetTextColor(textColor)
            hoursText.Text <- ""

        let point = new Point()
        activity.WindowManager.DefaultDisplay.GetSize(point)

        layout.SetPadding(10, 10, 10, 10)
        layout.LayoutParameters <- new ViewGroup.LayoutParams(point.X / 7, 128)

        layout :> View

    override this.Count = if entries <> null && entries.Length > 0 then 42 else 0

[<Activity (Label = "Overview")>]
type OverviewActivity() as this =
    inherit FSActivity<Model>()

    let dailyEntriesAdapter = new DailyEntriesAdapter(this, Array.empty)
    let dateGrid = this.LazyFindViewById<GridView>(Resource_Id.DateGrid)
    let initLoad = let now = DateTime.Now.Date
                   new BehaviorSubject<_>(LoadData (now.Year, now.Month))

    override this.InitModel () = failwith "model must be passed by previous activity"

    override this.OnCreate (bundle) =
        base.OnCreate(bundle)
        this.SetContentView (Resource_Layout.Overview)
        dateGrid.Value.Adapter <- dailyEntriesAdapter


    override this.OnResume() =
        base.OnResume()

        let updateUI model =
            if dailyEntriesAdapter.Entries <> model.dayEntries then
                dailyEntriesAdapter.Entries <- model.dayEntries

        this.Run update initLoad updateUI

    override this.OnPause () =
        base.OnPause()