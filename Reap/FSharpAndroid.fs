﻿namespace Reap

open Android.App
open Android.Content
open Android.OS
open Android.Runtime
open Android.Views
open Android.Widget
open Android.Util
open Android.Content.PM

open System.Reactive.Subjects
open System.Reactive.Disposables
open Newtonsoft.Json

open FSharp.Control.Reactive

open Shared


module Observable =
    let observeOnActivity (obs: System.IObservable<'t>) =
        Observable.observeOnContext (Application.SynchronizationContext) obs

module Once =
    type SingleThreadedLazy<'a>(init: unit -> 'a) =
        let mutable value = None
        member this.Value = match value with
                            | None -> let v = init ()
                                      value <- Some v
                                      v
                            | Some v -> v

    let once f = new SingleThreadedLazy<_>(f)

module Db =
    open System
    open Android.Database.Sqlite

    let table = "models"

    type ModelDb(ctx: Context) =
        inherit SQLiteOpenHelper(ctx, "model.db", null, 1)

        override this.OnCreate(db) =
            sprintf """
            CREATE TABLE %s (
                id       INTEGER PRIMARY KEY AUTOINCREMENT,
                activity TEXT    UNIQUE NOT NULL,
                data     TEXT,
                created  DATETIME DEFAULT CURRENT_TIMESTAMP,
                modified DATETIME NOT NULL
            )""" table
            |> db.ExecSQL

        override this.OnUpgrade(db, oldVersion, newVersion) =
            sprintf "DROP TABLE IF EXISTS %s" table
            |> db.ExecSQL

        member this.SaveModel<'m>(activity: Activity, model: 'm) =
            use db = this.WritableDatabase
            let activityName = activity.GetType().FullName
            let modelData = JsonConvert.SerializeObject(model)
            let q = sprintf "INSERT OR REPLACE INTO %s (activity, data, modified) VALUES ('%s', '%s', datetime('now'))" table activityName modelData
            db.ExecSQL(q)

        member this.LoadModel<'m>(activity: Activity) =
            use db = this.ReadableDatabase
            let activityName = activity.GetType().FullName
            let q = sprintf "SELECT data FROM %s WHERE activity = '%s'" table activityName
            use cursor = db.RawQuery(q, null)
            if cursor.MoveToFirst() then
                let json = cursor.GetString(0)
                let model = JsonConvert.DeserializeObject<'m>(json)
                Some <| model
            else
                None
            
open Once

[<AbstractClass>]
type FSActivity<'model>() as this=
    inherit Activity ()

    let mutable disposer: CompositeDisposable = new CompositeDisposable()
    let mutable db = once (fun _ -> new Db.ModelDb(this))
    let mutable restoreSubject: BehaviorSubject<'model> = null
    let mutable model = None
    let mutable created = false

    abstract member InitModel: unit -> 'model

    member this.Restore = restoreSubject :> System.IObservable<'model> 

    member this.AutoDispose(disposable: System.IDisposable) =
        disposer.Add(disposable)

    member this.FSIntent<'a, 'm when 'a :> FSActivity<'m>> (model: 'm) =
        let intent = new Intent(this, typeof<'a>)
        let modelStr = JsonConvert.SerializeObject(model)
        intent.PutExtra("model", modelStr)

    member this.HandleError(ex: exn) =
        if Log.IsLoggable("FSActivity", LogPriority.Error) then
            Log.Error("FSActivity", ex.Message) |> ignore
        let toast = Toast.MakeText(this.ApplicationContext, sprintf "Error: %s" ex.Message, ToastLength.Short)
        toast.Show()

    member this.Run<'msg> (update: 'model -> 'msg -> 'model * MVUPattern.Cmd<'msg>) (messages: System.IObservable<'msg>) (updateui: 'model -> unit) =
        let label = try
                        let l = this.PackageManager.GetApplicationInfo(this.PackageName, enum 0).NonLocalizedLabel.ToString().Split('.')
                        Array.get l ((Array.length l) - 1)
                    with
                    | :? PackageManager.NameNotFoundException -> "App"
        let logMessages = messages |> Observable.map (fun msg ->
            if Log.IsLoggable(label, LogPriority.Debug) then
                Log.Debug(label, msg.ToString()) |> ignore
            msg)

        let (modelChanges, disposeRun) = MVUPattern.run (restoreSubject.Value) (this.Restore) update logMessages
        disposer.Add disposeRun

        modelChanges
        |> Observable.observeOnActivity
        |> Observable.subscribeWithError (fun m -> updateui m; model <- Some m) this.HandleError
        |> disposer.Add

    member this.RestoreModel(model: 'model) =
        if restoreSubject = null then
            restoreSubject <- new BehaviorSubject<_>(model)
        restoreSubject.OnNext(model)

    override this.OnCreate (bundle) =
        base.OnCreate(bundle)
        created <- true

        match bundle with
        | null ->
            if this.Intent.HasExtra("model") then
                let m = JsonConvert.DeserializeObject<'model>(this.Intent.GetStringExtra("model"))
                this.RestoreModel(m)
        | b -> let m = JsonConvert.DeserializeObject<'model>(b.GetString("model"))
               this.RestoreModel(m)

    override this.OnResume() =
        base.OnResume()
        if disposer.IsDisposed then
            disposer <- new CompositeDisposable()

        if not created || restoreSubject = null then 
            let m = match db.Value.LoadModel<'model>(this) with
                    | Some m -> m
                    | None -> this.InitModel()
            this.RestoreModel(m)

        created <- false

    override this.OnPause() =
        base.OnPause()
        if not disposer.IsDisposed then
            disposer.Dispose()
        model
        |> Option.iter (fun m -> db.Value.SaveModel(this, m))

    override this.OnSaveInstanceState (bundle) =
        base.OnSaveInstanceState(bundle)
        let modelState = JsonConvert.SerializeObject(model)
        bundle.PutString("model", modelState)

    override this.OnRestoreInstanceState (bundle) =
        base.OnRestoreInstanceState(bundle)
        if bundle.ContainsKey("model") then
            let model = JsonConvert.DeserializeObject<'model>(bundle.GetString("model"))
            restoreSubject.OnNext(model)

    override this.OnDestroy() =
        base.OnDestroy()
        if not disposer.IsDisposed then
            disposer.Dispose()

module FSActivity =
    open Once

    type FSActivityLauncher<'model>(ctx: Context, model: 'model) =
        member this.Intent<'a when 'a :> FSActivity<'model>>() =
            let intent = new Intent(ctx, typeof<'a>)
            intent.PutExtra("model", JsonConvert.SerializeObject(model))

    type Activity with
        member this.LazyFindViewById<'v when 'v :> View>(id: int) =
            new SingleThreadedLazy<'v>(fun _ -> this.FindViewById<'v>(id))

        member this.ForModel<'m>(model: 'm) = new FSActivityLauncher<_>(this, model)

    type TextView with
        member this.SetChangedText(s) =
            if this.Text <> s then
                this.Text <- s

        member this.TextChanges =
            this.TextChanged
            |> Observable.map (fun args -> 
                let sb = new System.Text.StringBuilder()
                for c in args.Text do
                    sb.Append(c) |> ignore
                sb.ToString())
            |> Observable.distinctUntilChanged