﻿[<RequireQualifiedAccess>]
module Reap.Login

open System
open System.Text
open System.Net.Http
open System.Net.Http.Headers

open Android.App
open Android.Content
open Android.OS
open Android.Runtime
open Android.Views
open Android.Widget
open Android.Util

open System.Reactive.Disposables
open System.Reactive.Subjects
open FSharp.Control.Reactive
open Newtonsoft.Json
open System.Reactive.Concurrency

open FSActivity
open Shared
open MVUPattern

type AuthStatus =
    | NoAuth
    | AuthInProgress
    | AuthFailed of string
    | AuthSuccess of Harvest.Credentials

type Model = {username:string; password:string; authStatus:AuthStatus}

type Msg =
    | ModelReloaded of Model
    | UserNameChanged of string
    | PasswordChanged of string
    | Authenticate
    | AuthenticationChanged of AuthStatus

let private authenticate user password =
    async {
        let! credentialsOpt = Harvest.authenticate user password
        return match credentialsOpt with
               | Some creds -> AuthenticationChanged (AuthSuccess creds)
               | None -> AuthenticationChanged (AuthFailed "Login failed")
    }

let update model msg =
    match msg with
    | ModelReloaded m -> (m, NoCmd)
    | UserNameChanged name -> ({model with username=name}, NoCmd)
    | PasswordChanged password -> ({model with password=password}, NoCmd)
    | Authenticate -> ({model with authStatus=AuthInProgress}, Execute ((fun ex -> AuthenticationChanged (AuthFailed ex.Message)), (authenticate model.username model.password)))
    | AuthenticationChanged status -> ({model with authStatus = status}, NoCmd)

[<Activity (Label = "Reap", MainLauncher = true)>]
type LoginActivity () as this =
    inherit FSActivity<Model> ()

    let buildNumber = this.LazyFindViewById<TextView>(Resource_Id.BuildNumber)
    let userField = this.LazyFindViewById<EditText>(Resource_Id.LoginUserName)
    let button = this.LazyFindViewById<Button>(Resource_Id.AuthenticateButton)
    let passwordField = this.LazyFindViewById<EditText>(Resource_Id.LoginPassword)
    let loginStatus = this.LazyFindViewById<TextView>(Resource_Id.LoginStatus)

    override this.InitModel () = {username="joni.katajamaki@wunderdog.fi"; password="M2o8rMcM8Jb8"; authStatus=NoAuth}

    override this.OnCreate (bundle) =
        base.OnCreate (bundle)

        this.SetContentView (Resource_Layout.Main)

        let version = BuildTime.linkerTimestamp.ToString("H:mm:ss")
        buildNumber.Value.Text <- sprintf "Build time: %s" version

    override this.OnResume() =
        base.OnResume()

        let loginClicks = button.Value.Click
                          |> Observable.map (fun _ -> Authenticate)

        let userChange = userField.Value.TextChanges
                         |> Observable.map UserNameChanged

        let passwordChange = passwordField.Value.TextChanges
                             |> Observable.map PasswordChanged

        let modelRestore = this.Restore |> Observable.map ModelReloaded

        let messages = Observable.mergeSeq [ loginClicks; userChange; passwordChange; modelRestore ]

        let updateWidgets model =
            userField.Value.SetChangedText(model.username)
            passwordField.Value.SetChangedText(model.password)
            match model.authStatus with
            | NoAuth -> loginStatus.Value.SetChangedText("Not logged in")
            | AuthFailed message -> loginStatus.Value.SetChangedText(message)
            | AuthInProgress -> loginStatus.Value.SetChangedText("Logging in ...")
            | AuthSuccess creds ->
                loginStatus.Value.SetChangedText("Logged in")
                let overviewModel: Overview.Model = { credentials=creds
                                                    ; dayEntries=Array.empty
                                                    ; loadStatus=Overview.NotLoading
                                                    }
                this.ForModel(overviewModel).Intent<Overview.OverviewActivity>()
                |> this.StartActivity

        this.Run update messages updateWidgets
