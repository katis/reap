﻿module Shared.MVUPattern

open FSharp.Control.Reactive

open System.Threading
open System.Reactive.Disposables
open System.Reactive.Subjects

type Cmd<'t> =
    | Execute of (exn -> 't) * Async<'t>
    | NoCmd

type private ProcessorMsg<'msg> =
    | Msg of 'msg
    | Die

let run (initial: 'm) (reset: System.IObservable<'m>) (update: 'm -> 'msg -> ('m * Cmd<'msg>)) (messages: System.IObservable<'msg>) =
    let behavior = new BehaviorSubject<'m>(initial)

    let runTask (mb: MailboxProcessor<ProcessorMsg<'msg>>) (task: Async<'msg>) catch (canceller: CancellationToken) =
        async {
            try
                let! msg = task
                mb.Post(Msg msg)
            with
            | ex -> mb.Post(Msg <| catch ex)
        }
        |> fun task -> Async.Start(task, canceller)

    let mb = new MailboxProcessor<ProcessorMsg<'msg>>(fun box ->
        let canceller = new CancellationTokenSource()
        let rec loop model = async {
            let! msg = box.Receive()
            match msg with
            | Die ->
                canceller.Cancel()
                return ()
            | Msg contents -> 
                let (updatedModel, cmd) = update model contents

                match cmd with
                | Execute (catch, task) -> runTask box task catch canceller.Token
                | NoCmd -> ()

                behavior.OnNext(updatedModel)
                return! loop updatedModel
        }
        loop initial)
    mb.Start ()
    
    let disposable = new CompositeDisposable()

    let mbDisposer = { new System.IDisposable with
                        member this.Dispose() = mb.Post(Die)}
    disposable.Add(mbDisposer)

    messages
    |> Observable.subscribe (fun msg -> mb.Post(Msg msg))
    |> disposable.Add

    (behavior :> System.IObservable<'m>, disposable :> System.IDisposable)
