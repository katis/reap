﻿namespace Reap

open Android.App
open Android.Content
open Android.OS
open Android.Runtime
open Android.Views
open Android.Widget
open Android.Util

module BuildTime =
    let private parseLinkerTimestamp () =
        let c_PeHeaderOffset = 60
        let c_LinkerTimestampOffset = 8
        let filePath = System.Reflection.Assembly.GetCallingAssembly().Location
        let b : byte array = Array.zeroCreate 2048
        use s = new System.IO.FileStream(filePath, System.IO.FileMode.Open, System.IO.FileAccess.Read)
        s.Read(b, 0, 2048) |> ignore
        let i = System.BitConverter.ToInt32(b, c_PeHeaderOffset)
        let secondsSince1970 = System.BitConverter.ToInt32(b, i + c_LinkerTimestampOffset)
        let dt = System.DateTime(1970, 1, 1, 0, 0, 0)
        let dt1 = dt.AddSeconds(float(secondsSince1970))
        dt1.AddHours(float(System.TimeZone.CurrentTimeZone.GetUtcOffset(dt1).Hours))

    let private lazyLinkerTimestamp = lazy parseLinkerTimestamp ()

    let linkerTimestamp = lazyLinkerTimestamp.Force ()

module Log =
    type Logger (tag) =
        [<CustomOperation(name="debug", MaintainsVariableSpace=true)>]
        member this.Debug(a, [<ProjectionParameter>] b: unit -> string) =
            if Log.IsLoggable(tag, LogPriority.Debug) then
                Log.Debug(tag, b ()) |> ignore

        [<CustomOperation(name="info")>]
        member this.Info(a, [<ProjectionParameter>] b: unit -> string) =
            if Log.IsLoggable(tag, LogPriority.Info) then
                Log.Info(tag, b ()) |> ignore

        [<CustomOperation(name="warn")>]
        member this.Warn(a, [<ProjectionParameter>] b: unit -> string) =
            if Log.IsLoggable(tag, LogPriority.Warn) then
                Log.Warn(tag, b ()) |> ignore

        [<CustomOperation(name="error")>]
        member this.Error(a, [<ProjectionParameter>] b: unit -> string) =
            if Log.IsLoggable(tag, LogPriority.Error) then
                Log.Error(tag, b ()) |> ignore

        member this.Yield(v) = ()

    let log = new Logger("Reap")

    let s = sprintf