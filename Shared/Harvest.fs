﻿module Shared.Harvest

open System
open System.Text
open System.Net.Http
open System.Net.Http.Headers

open Newtonsoft.Json

type DailyEntry =
    { id:uint32
    ; user_id: uint32
    ; spent_at: DateTime
    ; created_at: DateTime
    ; updated_at: DateTime
    ; project_id: string
    ; task_id: string
    ; project: string
    ; task: string
    ; client: string
    ; notes: string
    ; hours_without_timer: float
    ; hours: float
    }

type Task =
    { id: uint32
    ; name: string
    ; billable: bool
    }

type Project =
    { id: uint32
    ; name: string
    ; billable: bool
    ; code: string
    ; client: string
    ; client_id: uint32
    ; client_currency: string
    ; client_currency_symbol: string
    ; tasks: Task list
    }

type DailyEntries =
    { for_day:DateTime
    ; day_entries: DailyEntry list
    ; projects: Project list
    }

// Type is public, innards are private
[<CLIMutable>]
type Credentials = private
                    { [<JsonProperty>] user:string
                    ; [<JsonProperty>] password:string
                    }

let private httpClient creds =
    let c = new HttpClient()
    c.BaseAddress <- new Uri("https://wunderdog.harvestapp.com")
    c.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"))
    let authString = sprintf "%s:%s" creds.user creds.password
                     |> Encoding.UTF8.GetBytes
                     |> Convert.ToBase64String
    c.DefaultRequestHeaders.Authorization <- new AuthenticationHeaderValue("Basic", authString)
    c

let authenticate user password =
    async {
        let creds = {user=user; password=password}
        use client = httpClient creds
        let! response = Async.AwaitTask <| client.GetAsync("/account/who_am_i")

        return if response.IsSuccessStatusCode then Some creds else None
    }

let dayEntries creds (date: DateTime) =
    async {
        use client = httpClient creds
        let! response = Async.AwaitTask <| client.GetAsync(sprintf "/daily/%d/%d" (date.DayOfYear) (date.Year))
        let! responseBody = Async.AwaitTask <| response.Content.ReadAsStringAsync()

        return JsonConvert.DeserializeObject<DailyEntries>(responseBody)
    }